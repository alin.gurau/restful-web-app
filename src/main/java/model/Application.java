package model;

import entities.Employee;
import entities.Workplace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import repositories.AddressRepository;
import repositories.EmployeeRepository;
import repositories.WorkplaceRepository;

import java.util.Arrays;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner init(WorkplaceRepository workplaceRepository,
                           EmployeeRepository employeeRepository) {
        return args ->
                Arrays.asList("jhoeller", "dsyer", "pwebb", "ogierke", "rwinch", "mfisher", "mpollack", "jlong")
                        .forEach(username -> {
                            Workplace workplace = workplaceRepository.save(new Workplace("companyName"));
                            employeeRepository.save(new Employee(workplace, "http://employee.com/1/" + username, "A description"));
                            employeeRepository.save(new Employee(workplace, "http://employee.com/2/" + username, "A description"));
                        });
    }

}
