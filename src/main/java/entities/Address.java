package entities;


import javax.persistence.*;
import java.util.Set;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String street;
    private int number;
    private String town;

    @ManyToMany(mappedBy = "addresses")
    private Set<Address> addresses;

    public Address(String street) {
        this.street = street;

    }

    public Address(String street, int number, String town, Set<Address> addresses) {
        this.street = street;
        this.number = number;
        this.town = town;
        this.addresses = addresses;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
}
