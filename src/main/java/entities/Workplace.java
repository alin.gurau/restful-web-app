package entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Workplace {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String companyName;

    @OneToMany(mappedBy = "workplace")
    private Set<Employee> employees = new HashSet<>();


    private Workplace() {

    }

    public Workplace(final String companyName) {
        this.companyName = companyName;
    }

    public Long getId() {
        return id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }
}
