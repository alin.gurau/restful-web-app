package entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String secondName;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Address> addresses;


    @ManyToOne
    private Workplace workplace;

    private Employee(Object workplace, String firstName, String secondName, Object addresses) {

    }

    public Employee(final Workplace workplace, final String firstName, final String secondName) {
        this.workplace = workplace;
        this.firstName = firstName;
        this.secondName = secondName;

    }

    public static Employee from(Workplace workplace, Employee employee) {
        return new Employee(workplace, employee.firstName, employee.secondName);
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public Workplace getWorkplace() {
        return workplace;
    }
}
