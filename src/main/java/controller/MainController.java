package controller;


import entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import repositories.AddressRepository;
import repositories.EmployeeRepository;
import repositories.WorkplaceRepository;

import java.net.URI;
import java.util.Collection;

import static javafx.scene.input.KeyCode.X;

@RestController
@RequestMapping("/employees/{workplaceId}")
public class MainController {

    private final EmployeeRepository employeeRepository;
    private final WorkplaceRepository workplaceRepository;
    private final AddressRepository addressRepository;


    @GetMapping
    Collection<Employee> readEmployees(@PathVariable String workplaceId) {
        this.validateWorkplace(workplaceId);

        return this.employeeRepository.findByWorkplaceCompanyName(workplaceId);
    }

    @PostMapping
    ResponseEntity<?> add(@PathVariable String workplaceId, @RequestBody Employee input) {
        this.validateWorkplace(workplaceId);

        return this.workplaceRepository
                .findByCompanyName(workplaceId)
                .map(workplace -> {
                    Employee result = this.employeeRepository.save(new Employee(workplace,
                            input.getFirstName(), input.getSecondName(), input.getAddresses()));

                    URI location = ServletUriComponentsBuilder
                            .fromCurrentRequest()
                            .path("/{id}")
                            .buildAndExpand(result.getId())
                            .toUri();

                    return ResponseEntity.created(location).build();
                })
                .orElse(ResponseEntity.noContent().build());
    }

    @GetMapping("/{employeeId}")
    Employee readEmployee(@PathVariable String workplaceId, @PathVariable Long employeeId) {
        this.validateWorkplace(workplaceId);

        return this.employeeRepository
                .findById(employeeId)
                .orElseThrow(() -> {
                    return new EmployeeNotFoundException(employeeId);
                });
    }


    private void validateWorkplace(String workplaceId) {
        this.workplaceRepository
                .findByCompanyName(workplaceId)
                .orElseThrow(() -> {
                    return new WorkplaceNotFoundException(workplaceId);
                });
    }

    private class EmployeeNotFoundException extends X {
        public EmployeeNotFoundException(Long employeeId) {
        }
    }
}
