package repositories;

import entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Collection<Employee> findByWorkplaceCompanyName(String workplaceId);

    <T> Optional<T> findById(Long employeeId);
}
